//Este programa muestra una suma en cadena desde el 0 hasta el número escrito por el usuario//
public class Sumaencadena{
	public static void main(String args[]){
		int n = Integer.parseInt(args[0]);
		int i = 1;
		int suma = 0;

		if(n>=0){
			while (i<=n && n>=0){
				suma = suma + i;
				i++;
			}System.out.println("La suma de todos los números comprendidos entre 0 es " +suma);
		}else{
				System.out.println("El número debe ser positivo");
				System.exit (1);
		}
	}
}